﻿namespace CNA_Tema2
{
	public class Sign
	{
		public string Name { get; }
		public int StartMonth { get; }
		public int StartDay { get; }
		public int EndMonth { get; }
		public int EndDay { get; }

		public Sign(string name, int startMonth, int startDay, int endMonth, int endDay)
		{
			Name = name;
			StartMonth = startMonth;
			StartDay = startDay;
			EndMonth = endMonth;
			EndDay = endDay;
		}

		public Sign()
		{
			Name = "NULL";
			StartMonth = 0;
			StartDay = 0;
			EndMonth = 0;
			EndDay = 0;
		}
	}
}