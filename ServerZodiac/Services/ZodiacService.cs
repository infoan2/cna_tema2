﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace CNA_Tema2
{
	public class GreeterService : Zodiac.ZodiacBase
	{
		private readonly string signsFilePath = "Signs.json";

		private readonly ILogger<GreeterService> _logger;

		public GreeterService(ILogger<GreeterService> logger)
		{
			_logger = logger;
		}

		public override Task<DateReply> SendDate(DateRequest request, ServerCallContext context)
		{
			return Task.FromResult(new DateReply
			{
				ZodiacSign = GetSignFromDate(request.Date)
			});
		}

		private string GetSignFromDate(Date date)
		{
			string jsonString = File.ReadAllText(signsFilePath);
			List<Sign> signs = JsonSerializer.Deserialize<List<Sign>>(jsonString);

			foreach (Sign sign in signs)
			{
				if (date.Month == sign.EndMonth)
				{
					if (date.Day <= sign.EndDay)
					{
						return sign.Name;
					}
				}
				else if (date.Month == sign.StartMonth)
				{
					if (date.Day >= sign.StartDay)
					{
						return sign.Name;
					}
				}
			}

			Console.ForegroundColor = ConsoleColor.Red;
			Console.WriteLine("Something went wrong. The received date is not part of any zodiac sign.");
			Console.ForegroundColor = ConsoleColor.Gray;
			
			return "SIGN NOT FOUND";
		}
	}
}