﻿using System;
using System.Threading.Tasks;
using CNA_Tema2;
using Grpc.Net.Client;

namespace ClientZodiac
{
	class Program
	{
		static async Task Main(string[] args)
		{
			using GrpcChannel channel = GrpcChannel.ForAddress("https://localhost:5001");
			Zodiac.ZodiacClient client = new Zodiac.ZodiacClient(channel);

			Console.Write("Date of birth: ");
			string dateString = Console.ReadLine()?.Trim();

			if (dateString == null || !DataValidation.IsDataValid(dateString))
			{
				Console.WriteLine("Data input is invalid. The program is stopping...");
				return;
			}

			DateTime parsedDate = DateTime.Parse(dateString);

			DateReply response = await client.SendDateAsync(new DateRequest
			{
				Date = new Date
				{
					Year = parsedDate.Year,
					Month = parsedDate.Month,
					Day = parsedDate.Day
				}
			});
			
			Console.WriteLine($"The returned sign: {response.ZodiacSign}");
			Console.WriteLine("Press any key to exit...");
			Console.ReadKey();
		}
	}
}