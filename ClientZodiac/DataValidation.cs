﻿using System;

namespace ClientZodiac
{
	public static class DataValidation
	{
		public static bool IsDataValid(string dateString)
		{
			if (!DateTime.TryParse(dateString, out DateTime _))
			{
				return false;
			}

			return true;
		}
	}
}